package org.acme.client.entity;

import io.quarkus.mongodb.panache.common.MongoEntity;
import org.bson.BsonType;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.codecs.pojo.annotations.BsonRepresentation;

@MongoEntity(collection = "ecommerce")
public class Client
{
	@BsonId
	@BsonRepresentation(BsonType.OBJECT_ID)
	public String id;

	@BsonProperty("firstname")
	public String firstName;

	@BsonProperty("lastname")
	public String lastName;

	@BsonProperty("eta")
	public int eta;

	public Client()
	{
	}

	public
	Client(String id, String firstName, String lastName, int eta)
	{
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.eta = eta;
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public
	void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public int getEta()
	{
		return eta;
	}

	public void setEta(int eta)
	{
		this.eta = eta;
	}
}
