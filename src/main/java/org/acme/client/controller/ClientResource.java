package org.acme.client.controller;

import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.acme.client.entity.Client;
import org.acme.client.service.ClientService;

import java.util.List;

@Path("/clients")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ClientResource
{
	@Inject
	ClientService clientService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello RESTEasy";
    }

	@POST
	public Response addClient(Client client)
	{
		clientService.createUser(client);
		return Response.ok(Response.Status.CREATED).build();
	}

	@GET
	@Path("/{id}")
	public Client getClient(@PathParam("id") String id){
		return clientService.getClientById(id);
	}

	@GET
	@Path("/all")
	public List<Client> getClients(){
		return clientService.AllClients();
	}

	@PUT
	@Path("/{id}")
	public Response updateClient(@PathParam("id") String id, Client client){
		if(clientService.updateClient(id,client)){
			return Response.status(Response.Status.CREATED).build();
		}else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}

	@DELETE
	@Path("/{id}")
	public Response deleteClient(@PathParam("id") String id){
		if(clientService.deleteCliente(id)){
			return Response.status(Response.Status.NO_CONTENT).build();
		}else{
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
	}
}
