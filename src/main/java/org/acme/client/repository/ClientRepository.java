package org.acme.client.repository;

import io.quarkus.mongodb.panache.PanacheMongoRepository;
import jakarta.enterprise.context.ApplicationScoped;
import org.acme.client.entity.Client;
import org.bson.types.ObjectId;

import java.util.Optional;

@ApplicationScoped
public class ClientRepository implements PanacheMongoRepository<Client>
{
	public Optional<Client> findByClientId(String id){
		return findByIdOptional(new ObjectId(id));
	}
}
