package org.acme.client.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.cache.CacheKey;
import io.quarkus.panache.common.Sort;
import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.keys.KeyCommands;
import io.quarkus.redis.datasource.value.ValueCommands;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.acme.client.entity.Client;
import org.acme.client.repository.ClientRepository;

import java.util.List;
import java.util.Optional;
@ApplicationScoped
public class ClientService
{
	@Inject
	ClientRepository clientRepository;

	private static final String CLIENT_CACHE_GET = "client: ";

	private final KeyCommands<String> keyCommands;
	private final ValueCommands<String,String> valueCommands;

	public ClientService(RedisDataSource ds){
		keyCommands = ds.key();
		valueCommands = ds.value(String.class, String.class);
	}

	public void createUser(Client client)
	{
		clientRepository.persist(client);
		valueCommands.set(CLIENT_CACHE_GET + client,serialise(client));
	}

	public Client getClientById(@CacheKey String id)
	{
		String cacheKey = CLIENT_CACHE_GET + id;
		String cachedClient = valueCommands.get(cacheKey);

		if (cachedClient != null)
		{
			// Log cache hit
			//logger.info("Cache hit for client id: " + id);
			return deserialize(cachedClient);
		}

		// Log cache miss
		//logger.info("Cache miss for client id: " + id);

		Optional<Client> clientOptional = clientRepository.findByClientId(id);

		if (clientOptional.isPresent())
		{
			Client client = clientOptional.get();
			// Serialize and cache the client with an expiry time, e.g., 1 hour
			valueCommands.set(cacheKey, serialise(client));
			return client;
		}
		else
		{
			throw new RuntimeException("Client with id " + id + " not found");
		}
	}

	public List<Client> AllClients(){
		return clientRepository.listAll(Sort.by("age"));
	}

	public Boolean updateClient(String id, Client client){
		Client clientOptional = getClientById(id);
		if(clientOptional !=null){
			client.id = clientOptional.id;
			clientRepository.update(client);
			valueCommands.set(CLIENT_CACHE_GET+id,serialise(client));
			return true;
		}else {
			return false;
		}
	}

	public Boolean deleteCliente(String id){
		Client client = getClientById(id);
		if(client!=null)
		{
			clientRepository.delete(client);
			keyCommands.del(CLIENT_CACHE_GET + id);
			return true;
		}else{
			return false;
		}
	}

	private String serialise(Client client)
	{
		ObjectMapper objectMapper = new ObjectMapper();
		try
		{
			return objectMapper.writeValueAsString(client);
		}
		catch (JsonProcessingException e)
		{
			throw new RuntimeException(e);
		}
	}

	private Client deserialize(String cachedClient)
	{
		ObjectMapper objectMapper = new ObjectMapper();
		try
		{
			return objectMapper.readValue(cachedClient, Client.class);
		}
		catch (JsonProcessingException e)
		{
			throw new RuntimeException(e);
		}
	}

}
