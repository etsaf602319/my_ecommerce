package org.acme.client;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class ClientResourceTest
{
    @Test
    void testHelloEndpoint() {
        given()
          .when().get("/clients")
          .then()
             .statusCode(200)
             .body(is("Hello RESTEasy"));
    }

}